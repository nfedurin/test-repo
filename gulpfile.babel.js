const gulp = require('gulp');
const plugin = require('gulp-load-plugins')();

// This is transpiler, convert new JS into old JS

gulp.task('build',() =>
  gulp.src('src/wishmaster.js')
    .pipe(plugin.babel({ presets: ['react', 'es2015'] }))
    // Plugin which build all in one file
    .pipe(plugin.browserify({ insertGlobals: false }))
    .pipe(plugin.uglify())
    .pipe(gulp.dest('public'))
);
