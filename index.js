// dependencies
const Koa = require('koa');
const logger = require('koa-logger');
const serve = require('koa-static');
const router = require('koa-router')();

// server
const server = new Koa();

server
  .use(logger())
  // static files
  .use(serve('public'))
  // router middlewae
  .use(router.routes())
  // listen on 3000 port
  .listen(3000);

//router
router.get('/thread', (ctx, next) => {
  ctx.body = [
    {
      label: '10chan here',
      text: 'yaya'
    },
    {
      label: '10chan here. Again',
      text: 'yaya x2'
    }
  ];
});
